#!/usr/bin/make -f

PYTHONS:=$(shell pyversions -vr)
PYTHON3S:=$(shell py3versions -vr)

UPSTREAM_GIT = hg::https://bitbucket.org/haypo/aioeventlet
-include /usr/share/openstack-pkg-tools/pkgos.make

%:
	dh $@ --buildsystem=python_distutils --with python2,python3,sphinxdoc

override_dh_install:
	set -e ; for pyvers in $(PYTHONS); do \
		python$$pyvers setup.py install --install-layout=deb \
			--root $(CURDIR)/debian/python-aioeventlet; \
	done
	set -e ; for pyvers in $(PYTHON3S); do \
		python$$pyvers setup.py install --install-layout=deb \
			--root $(CURDIR)/debian/python3-aioeventlet; \
	done
	rm -rf $(CURDIR)/debian/python*-aioeventlet/usr/lib/python*/dist-packages/*.pth

override_dh_auto_test:
ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	set -e ; for pyvers in $(PYTHONS) $(PYTHON3S) ; do \
		PYMAJOR=`echo $$pyvers | cut -d'.' -f1` ; \
		echo "===> Testing with python$$pyvers (python$$PYMAJOR)" ; \
		python$$pyvers runtests.py -r -v ; \
		find  . -iname '*.pyc' -delete ; \
	done
endif

override_dh_clean:
	dh_clean -O--buildsystem=python_distutils
	rm -rf build

override_dh_sphinxdoc:
	sphinx-build -b html doc debian/python-aioeventlet-doc/usr/share/doc/python-aioeventlet-doc/html
	dh_sphinxdoc -O--buildsystem=python_distutils

# Commands not to run
override_dh_installcatalogs:
override_dh_installemacsen override_dh_installifupdown:
override_dh_installinfo override_dh_installmenu override_dh_installmime:
override_dh_installmodules override_dh_installlogcheck:
override_dh_installpam override_dh_installppp override_dh_installudev override_dh_installwm:
override_dh_installxfonts override_dh_gconf override_dh_icons override_dh_perl override_dh_usrlocal:
override_dh_installcron override_dh_installdebconf:
override_dh_installlogrotate override_dh_installgsettings:

